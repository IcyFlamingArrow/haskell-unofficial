Title: dev-scm/git-annex without manpages
Author: Nicolas Braud-Santoni <nicolas@braud-santoni.eu>
Content-Type: text/plain
Posted: 2013-09-30
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-scm/git-annex

Because of Hackage's temporary breakage (on tarballs bigger than 1MB),
dev-scm/git-annex is currently shipped without the documentation's Markdown
files.

They can be recieved from git-annex's Git repository.
