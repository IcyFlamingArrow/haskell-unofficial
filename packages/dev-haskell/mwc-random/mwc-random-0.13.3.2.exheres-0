# Copyright 2010 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Generates high quality random numbers"
DESCRIPTION="
This package contains code for generating high quality random numbers that follow either a uniform
or normal distribution. The generated numbers are suitable for use in statistical applications.

The uniform PRNG uses Marsaglia's MWC256 (also known as MWC8222) multiply-with-carry generator,
which has a period of 2^8222 and fares well in tests of randomness. It is also extremely fast,
between 2 and 3 times faster than the Mersenne Twister.

Compared to the mersenne-random package, this package has a more convenient API, is faster, and
supports more statistical distributions.
"

LICENCES="BSD-3"
PLATFORMS="~amd64"
MYOPTIONS=""

# Due to cyclic dependencies on statistics when tests are enabled
RESTRICT="test"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/primitive
        dev-haskell/time
        dev-haskell/vector[>=0.7]
    ")

    $(haskell_test_dependencies "
        dev-haskell/HUnit
        dev-haskell/QuickCheck
        dev-haskell/statistics[>=0.10.1.0]
        dev-haskell/test-framework
        dev-haskell/test-framework-hunit
        dev-haskell/test-framework-quickcheck2
    ")
"


