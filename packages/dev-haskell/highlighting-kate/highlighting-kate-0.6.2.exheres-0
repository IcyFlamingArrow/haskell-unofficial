# Copyright 2012 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require hackage [ has_bin="true" ]

SUMMARY="A syntax highlighting library with support for nearly one hundred languages."
DESCRIPTION="A syntax highlighting library with support for nearly one hundred languages. The syntax parsers are
automatically generated from Kate syntax descriptions (<http://kate-editor.org/>), so any syntax
supported by Kate can be added. An (optional) command-line program is provided, along with a
utility for generating new parsers from Kate XML syntax descriptions."

LICENCES="GPL-2"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/blaze-html[>=0.4.2&<0.9]
        dev-haskell/bytestring
        dev-haskell/containers
        dev-haskell/filepath
        dev-haskell/mtl
        dev-haskell/parsec
        dev-haskell/pcre-light[>=0.4&<0.5]
        dev-haskell/regex-pcre-builtin[>=0.94.4.8.8.35]
        dev-haskell/utf8-string
    ")
    $(haskell_bin_dependencies "
        dev-haskell/filepath
    ")
"

CABAL_SRC_CONFIGURE_PARAMS=(
    --flags=executable
)

src_install() {
    cabal_src_install

    alternatives_for ${PN} ${SLOT} 1 \
        /usr/$(exhost --target)/bin/Highlight Highlight-${SLOT}
}

RESTRICT="test"

